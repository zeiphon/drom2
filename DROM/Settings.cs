﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DROM
{
    public class Settings
    {

        /* Program Option fields */
        public bool bAutoConnect
        { get; set; }
        public bool bFlashOnMsg
        { get; set; }
        public bool bSpellcheck
        { get; set; }
        public bool bRememberName
        { get; set; }

        public bool bPlaySounds
        { get; set; }
        public bool bPlayMsgActive
        { get; set; }
        public bool bPlayNudges
        { get; set; }


        public string strUsername
        { get; set; }
        public string strServer
        { get; set; }
        public string strDRPath
        { get; set; }

        /* Settings class fields */
        private IniFile m_ini;
        private bool m_bValid = false;

        public Settings()
        {

        }

        public bool setPath(string strPath)
        {
            bool bOK = false;
            try
            {
                new System.IO.FileInfo(strPath);
                bOK = true;
            }
            catch (ArgumentException)
            {
            }
            catch (System.IO.PathTooLongException)
            {
            }
            catch (NotSupportedException)
            {
            }

            m_bValid = bOK;

            if (!bOK)
                return false;

            bool bExists = System.IO.File.Exists(strPath);

            m_ini = new IniFile(strPath);

            if (!bExists) // Set default values
            {
                bAutoConnect = true;
                bRememberName = false;
                bFlashOnMsg = true;
                bSpellcheck = false;
                bPlaySounds = true;
                bPlayMsgActive = false;
                bPlayNudges = true;

                strDRPath = "";
                strServer = "vps.gr";

                Save();
            }

            return true;
        }

        public bool IsValid()
        {
            return m_bValid;
        }

        public bool Load()
        {
            if (!m_bValid || m_ini == null)
                return false;

            bAutoConnect = m_ini.ReadValue("options", "autoconnect").Equals("true", StringComparison.CurrentCultureIgnoreCase);
            bFlashOnMsg = m_ini.ReadValue("options", "flashonmsg").Equals("true", StringComparison.CurrentCultureIgnoreCase);
            bSpellcheck = m_ini.ReadValue("options", "spellcheck").Equals("true", StringComparison.CurrentCultureIgnoreCase);
            bRememberName = m_ini.ReadValue("options", "remembername").Equals("true", StringComparison.CurrentCultureIgnoreCase);
            bPlaySounds = m_ini.ReadValue("options", "playsounds").Equals("true", StringComparison.CurrentCultureIgnoreCase);
            bPlayMsgActive = m_ini.ReadValue("options", "playmsgactive").Equals("true", StringComparison.CurrentCultureIgnoreCase);
            bPlayNudges = m_ini.ReadValue("options", "playnudges").Equals("true", StringComparison.CurrentCultureIgnoreCase);

            strUsername = m_ini.ReadValue("connection", "username");            
            strServer = m_ini.ReadValue("connection", "server");
            strDRPath = m_ini.ReadValue("darkreign", "runpath");

            return true;
        }

        public bool Save()
        {
            if (!m_bValid || m_ini == null)
                return false;


            m_ini.WriteValue("options", "autoconnect", bAutoConnect.ToString());
            m_ini.WriteValue("options", "flashonmsg", bFlashOnMsg.ToString());
            m_ini.WriteValue("options", "spellcheck", bSpellcheck.ToString());
            m_ini.WriteValue("options", "remembername", bRememberName.ToString());
            m_ini.WriteValue("options", "playsounds", bPlaySounds.ToString());
            m_ini.WriteValue("options", "playmsgactive", bPlayMsgActive.ToString());
            m_ini.WriteValue("options", "playnudges", bPlayNudges.ToString());

            m_ini.WriteValue("connection", "username", strUsername);
            m_ini.WriteValue("connection", "server", strServer);
            m_ini.WriteValue("darkreign", "runpath", strDRPath);

            return true;
        }

    }
}
