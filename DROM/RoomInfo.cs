﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DROM
{
    public class RoomInfo
    {
        public Session m_session;

        public RoomInfo(Session session)
        {
            m_session = session;
        }

        public override string ToString()
        {
            return (m_session.bInProgress ? "* " : "") + m_session.pName + " (" + m_session.iNumPlayers + "/" + m_session.iMaxPlayers + ")";
        }
    }
}
