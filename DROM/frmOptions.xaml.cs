﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace DROM
{
    /// <summary>
    /// Interaction logic for frmOptions.xaml
    /// </summary>
    public partial class frmOptions : Window
    {
        private Settings m_Settings;

        public frmOptions(Settings settings)
        {
            InitializeComponent();

            m_Settings = settings;

            chkAutoConnect.IsChecked = m_Settings.bAutoConnect;
            chkFlash.IsChecked = m_Settings.bFlashOnMsg;
            chkSpellcheck.IsChecked = m_Settings.bSpellcheck;
            chkRememberName.IsChecked = m_Settings.bRememberName;
            chkPlaySounds.IsChecked = m_Settings.bPlaySounds;
            chkPlayMsgActive.IsChecked = m_Settings.bPlayMsgActive;
            chkNudges.IsChecked = m_Settings.bPlayNudges;

            txtDRPath.Text = m_Settings.strDRPath;

            rectBottom.Fill = SystemColors.ControlBrush;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!m_Settings.IsValid())
            {
                MessageBox.Show(this, "Error: Settings file is invalid", "DROM - Settings error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            m_Settings.bAutoConnect = (bool)chkAutoConnect.IsChecked;
            m_Settings.bFlashOnMsg = (bool)chkFlash.IsChecked;
            m_Settings.bSpellcheck = (bool)chkSpellcheck.IsChecked;
            m_Settings.bRememberName = (bool)chkRememberName.IsChecked;
            m_Settings.bPlaySounds = (bool)chkPlaySounds.IsChecked;
            m_Settings.bPlayMsgActive = (bool)chkPlayMsgActive.IsChecked;
            m_Settings.bPlayNudges = (bool)chkNudges.IsChecked;

            m_Settings.strDRPath = txtDRPath.Text;

            m_Settings.Save();

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            bool? res = ofd.ShowDialog(this);
            if (res == true)
            {
                if (File.Exists(ofd.FileName) || MessageBox.Show("The file does not seem to exist. Use anyway?", "DROM - Nonexistant file", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    txtDRPath.Text = ofd.FileName; 
            }
        }
    }
}
