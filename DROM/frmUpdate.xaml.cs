﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DROM
{
    /// <summary>
    /// Interaction logic for frmUpdate.xaml
    /// </summary>
    public partial class frmUpdate : Window
    {
        public frmUpdate()
        {
            InitializeComponent();

            chkNoNotify.Unchecked += new RoutedEventHandler(chkNoNotify_Unchecked);

            rectBottom.Fill = SystemColors.ControlBrush;
        }

        void chkNoNotify_Unchecked(object sender, RoutedEventArgs e)
        {
            btnOK.Content = "Okay";
        }

        private void chkNoNotify_Checked(object sender, RoutedEventArgs e)
        {
            btnOK.Content = "Jesus";
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
