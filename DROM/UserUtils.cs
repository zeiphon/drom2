﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows;

namespace DROM
{
    class ChatUtils
    {

        private System.Windows.Controls.RichTextBox m_rtb;
        private FlowDocument m_flowDocument;
        private Paragraph m_flowParagraph;

        public ChatUtils(System.Windows.Controls.RichTextBox rtb)
        {
            m_rtb = rtb;

            m_flowDocument = new FlowDocument();
            m_flowParagraph = new Paragraph();
            m_flowDocument.Blocks.Add(m_flowParagraph);
            m_rtb.Document = m_flowDocument;

            m_flowParagraph.Margin = new Thickness(0);
        }

        public void AddInline(Inline item)
        {
            m_flowParagraph.Inlines.Add(item);
            m_rtb.ScrollToEnd();
        }

        public void InsertTimestamp(SolidColorBrush brushTimestamp)
        {
            Run runTimestamp = new Run("[" + DateTime.Now.ToString("HH:mm") + "] ");
            runTimestamp.Foreground = brushTimestamp;

            AddInline(runTimestamp);
        }

        // Default calls
        public void InsertUsername(string strUsername, SolidColorBrush brushUser)
        {
            InsertUsername(strUsername, brushUser, true, false);
        }

        // Insert username with appropriate formatting
        public void InsertUsername(string strUsername, SolidColorBrush brushUser, bool bBold, bool bItalic)
        {
            String strName = strUsername;

            if (strUsername.StartsWith("Ð")) // Make sure prefix isn't highlighted
            {
                strName = strUsername.Substring(2);
                Inline runPrefix = new Run(strUsername.Substring(0, 2));
                
                // (Don't bold the prefix, unlike username)

                if (bItalic)
                    runPrefix = new Italic(runPrefix);
                AddInline(runPrefix);
            }

            Inline runUsername = new Run(strName);
            if (bBold)
                runUsername = new Bold(runUsername);
            if (bItalic)
                runUsername = new Italic(runUsername);

            runUsername.Foreground = brushUser;
            AddInline(runUsername);

        }

        public void InsertChatMessage(string strUsername, string strMessage, SolidColorBrush brushUser, SolidColorBrush brushMessage)
        {
            InsertTimestamp(Brushes.Black); // Default to black for the moment            

            Run runMessage = new Run(strMessage + "\n");
            runMessage.Foreground = brushMessage;

            InsertUsername(strUsername, brushUser);
            AddInline(new Run(": "));
            AddInline(runMessage);
        }

        public void InsertStatusMessage(string strMessage, SolidColorBrush brushMessage)
        {
            //InsertTimestamp(Brushes.Black); // Default to black for the moment            

            Bold runMessage = new Bold(new Run(strMessage + "\n"));
            runMessage.Foreground = brushMessage;

            AddInline(new Run(" ** "));
            AddInline(runMessage);
        }

        public void InsertJoinMessage(string strUser, SolidColorBrush brushUser, SolidColorBrush brushMessage)
        {
            InsertTimestamp(Brushes.Black); // Default to black for the moment            

            String strName = strUser;
            InsertUsername(strUser, Brushes.Blue, false, true);


            Italic runMessage = new Italic(new Run(" joined the room.\n"));
            runMessage.Foreground = brushMessage;

            //InsertUsername(strUser, brushUser);
            Italic runUser = new Italic(new Run(strName));
            runUser.Foreground = brushUser;
            AddInline(runMessage);
        }

        public void InsertLeaveMessage(string strUser, SolidColorBrush brushUser, SolidColorBrush brushMessage)
        {
            InsertTimestamp(Brushes.Black); // Default to black for the moment            

            String strName = strUser;
            InsertUsername(strUser, Brushes.Blue, false, true);

            Italic runMessage = new Italic(new Run(" left the room.\n"));
            runMessage.Foreground = brushMessage;

            //InsertUsername(strUser, brushUser);
            Italic runUser = new Italic(new Run(strName));
            runUser.Foreground = brushUser;
            AddInline(runMessage);
        }

    }
}
