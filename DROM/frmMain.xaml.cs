﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Media;
using System.IO;

namespace DROM
{

    // Bit info
    enum UserStatus { Available = 0, DND = 1, Away = 2, RTG = 3 };
    enum OptionBits { Sound = 0, Nudge = 1 };
    enum NudgeBits { Direct = 0, Info = 1, Disabled = 2 };

    /// <summary>
    /// Interaction logic for frmMain.xaml
    /// </summary>
    public partial class frmMain : Window
    {
        private ANet m_ANet;
        private ChatUtils m_Chat;

        private Settings m_Settings;

        private System.Timers.Timer m_Timer;
        private System.Timers.Timer m_CountTimer;

        /* State variables */
        int m_iID = -1; // ANet user ID
        int m_iStatus = 0;
        int m_iOptions = 0;

        /* Sounds */
        SoundPlayer m_sndInMsg;
        SoundPlayer m_sndOutMsg;
        SoundPlayer m_sndNudge;

        /* Icons/Images */
        BitmapImage m_imgUserBlue;
        BitmapImage m_imgUserRed;
        BitmapImage m_imgUserDel;
        BitmapImage m_imgFlagBlue;
        BitmapImage m_imgUserGreen;
        BitmapImage m_imgJoystick;
        BitmapImage m_imgSound;
        BitmapImage m_imgSoundMute;

        /* Callbacks */
        ConnectionCallback m_Conn_cb;
        ChatMessageCallback m_Chat_cb;
        ConnectionLostCallback m_ConnLost_cb;
        AddPlayerCallback m_AddPlayer_cb;
        DelPlayerCallback m_DelPlayer_cb;
        EnumSessionsCallback m_EnumSessions_cb;
        PingPlayerCallback m_PingPlayer_cb;
        EnumSessPlayersCallback m_EnumSessPlayers_cb;
        NudgeCallback m_Nudge_cb;
        InfoCallback m_Info_cb;
        IDCallback m_ID_cb;

        public frmMain()
        {
            InitializeComponent();

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();

            Version version = assembly.GetName().Version;

            this.Title = "DROM v" + version.Major + "." + version.Minor + "." + version.Build + " build " + version.Revision;
            //this.Title = "DROM 0.6 alpha";

            m_Chat = new ChatUtils(this.txtChat);
            
            mnuConnect.Click += mnuConnect_Click;
            mnuDisconnect.Click += mnuDisconnect_Click;
            mnuExit.Click += mnuExit_Click;
            mnuOptions.Click += new RoutedEventHandler(mnuOptions_Click);

            lstUsers.MouseDoubleClick += new MouseButtonEventHandler(lstUsers_MouseDoubleClick);
            lstRooms.MouseDoubleClick += new MouseButtonEventHandler(lstRooms_MouseDoubleClick);

            btnLaunch.Drop += new DragEventHandler(btnLaunch_Drop);
            AccessKeyManager.Register("\r", btnSend);

            m_Conn_cb = new ConnectionCallback(Connection);
            m_Chat_cb = new ChatMessageCallback(ChatMessageReceived);
            m_AddPlayer_cb = new AddPlayerCallback(AddPlayer);
            m_DelPlayer_cb = new DelPlayerCallback(DelPlayer);
            m_ConnLost_cb = new ConnectionLostCallback(ConnectionLost);
            m_EnumSessions_cb = new EnumSessionsCallback(EnumSessions);
            m_PingPlayer_cb = new PingPlayerCallback(PingPlayerCallback);
            m_EnumSessPlayers_cb = new EnumSessPlayersCallback(EnumSessPlayers);
            m_Nudge_cb = new NudgeCallback(Nudge_cb);
            m_Info_cb = new InfoCallback(Info_cb);
            m_ID_cb = new IDCallback(ID_cb);

            this.Loaded += frmMain_Loaded;
            this.Closing += frmMain_Closing;

            txtSend.SpellCheck.IsEnabled = false;

            m_Timer = new System.Timers.Timer(10000); // 10 seconds
            m_Timer.Elapsed += new System.Timers.ElapsedEventHandler(m_Timer_Elapsed);
            m_Timer.Enabled = true;

            m_CountTimer = new System.Timers.Timer(1000); // 1 second
            m_CountTimer.Elapsed += new System.Timers.ElapsedEventHandler(m_CountTimer_Elapsed);

            m_Settings = new Settings();

            string strPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            System.IO.Directory.CreateDirectory(strPath + "\\DROM");
            strPath += "\\DROM\\drom_settings.ini";

            m_Settings.setPath(strPath);
            m_Settings.Load(); // Perhaps shouldn't do this in the constructor..?

            m_sndInMsg = new SoundPlayer(@"data\rcvmsg.wav");
            m_sndOutMsg = new SoundPlayer(@"data\sndmsg.wav");
            m_sndNudge = new SoundPlayer(@"data\nudge.wav");

        }

        void btnLaunch_Drop(object sender, DragEventArgs e)
        {
            // TODO: Drop file on button??

        }

        private int iCount = 0;
        void m_CountTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                    if (iCount == 0 && lblCount.IsVisible)
                        lblCount.Visibility = Visibility.Hidden;
                    else
                        lblCount.Content = iCount + "..";
                }
            ));


            if (iCount == 0)
                iCount = 30;
            else
                iCount -= 1;
        }

        int Bits(int iBits)
        {
            return (1 << iBits);
        }

        void UpdateOptions()
        {
            if (btnMute != null)
                btnMute.IsChecked = !m_Settings.bPlaySounds;

            m_iOptions = 0;
            m_iOptions |= ((m_Settings.bPlaySounds ? 1 : 0) << (int)OptionBits.Sound);
            m_iOptions |= ((m_Settings.bPlayNudges ? 1 : 0) << (int)OptionBits.Nudge);

            if (m_ANet.isInited)
                m_ANet.anSetCliInfo(m_iStatus, m_iOptions);
        }

        void Mute(bool bMute)
        {
            m_Settings.bPlaySounds = !bMute;

            /*if (bMute)
            {
                m_iOptions |= 1;
            }
            else
            {
                m_iOptions &= ~(1);
            }*/

            UpdateOptions();
        }

        // In Do Not Disturb mode?
        bool IsDND()
        {
            return m_iStatus == (int)UserStatus.DND;
        }

        // Play sounds at all?
        bool PlaySounds()
        {
            return (m_Settings.bPlaySounds && !IsDND());
        }
        // Play incoming message sound?
        bool PlayInSound()
        {
            return (PlaySounds() && !m_Settings.bPlayMsgActive) || (PlaySounds() && m_Settings.bPlayMsgActive && !this.IsActive);
        }

        bool PlayOutSound()
        {
            return PlaySounds();
        }

        bool PlayNudge()
        {
            return m_Settings.bPlayNudges;
        }

        void lstRooms_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            RoomInfo room = (RoomInfo)lstRooms.SelectedItem;
            if (room == null)
                return;
            
            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                    m_ANet.anEnumSessPlayers(room.m_session.pName, m_EnumSessPlayers_cb);
                }
            ));
        }

        void lstUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            /*StackPanel sp = (StackPanel)lstUsers.SelectedItem;
            Label lbl = (Label)sp.Children[1];
            PlayerInfo player = (PlayerInfo)lbl.Content;
            if (player == null)
                return;
            player.iKarma = (short)(player.m_player.iID + player.iKarma + new Random().Next(50));

            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                    m_Chat.InsertStatusMessage("Pinging " + player.m_player.pName + "..", Brushes.MidnightBlue);
                    m_ANet.anPingPlayer(player.m_player.iID, player.iKarma, m_PingPlayer_cb);
                }
            ));*/

            /* DUPLICATED CODE! */
            // TODO: Factor out Info dialog code

            int iIndex = lstUsers.SelectedIndex;
            if (iIndex < 0 || iIndex > lstUsers.Items.Count - 1)
                return;

            PlayerInfo pi = GetPlayerInfo(lstUsers, iIndex);

            frmInfo infoForm = new frmInfo(pi);
            infoForm.Show();
        }

        void m_Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {

            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                    m_ANet.anEnumSessions(m_EnumSessions_cb);
                }
            ));
        }

        private void frmMain_Loaded(Object sender, EventArgs e)
        {
            m_CountTimer.Enabled = true;

            // Init images
            m_imgUserBlue = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/user.png"));
            m_imgUserRed = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/user_red.png"));
            m_imgUserDel = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/user_delete.png"));
            m_imgFlagBlue = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/flag_blue.png"));
            m_imgUserGreen = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/user_green.png"));
            m_imgJoystick = new BitmapImage(new Uri("pack://application:,,,/DROM;component/DROM.ico"));//Images/joystick.png"));
            m_imgSound = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/sound_none.png"));
            m_imgSoundMute = new BitmapImage(new Uri("pack://application:,,,/DROM;component/Images/sound_mute.png"));

            // Init ANet
            m_ANet = new ANet(this);
            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                Version version = assembly.GetName().Version;

                // Assuming 4-byte (32 bit) int
                int iCliVers = 0;
                iCliVers = version.Major << 8;
                iCliVers = (iCliVers + version.Minor) << 8;
                iCliVers = (iCliVers + version.Build) << 8;
                iCliVers = (iCliVers + version.Revision);

                /*string strCliVers = (iCliVer & 0xFF000000) + "." + (iCliVer & 0x00FF0000) + "." +
                    (iCliVer & 0x0000FF00) + " build " + (iCliVer & 0x000000FF);*/

                m_ANet.Init(iCliVers);
            }
            catch (Exception ex)
            {
                m_Chat.InsertStatusMessage("Error loading: " + ex.ToString(), Brushes.Red);
                m_Chat.AddInline(new Run("Please make sure that the anetwrap.dll and anetdll.dll library files are in the DROM directory, then restart DROM.\n"));
            }

            m_ANet.anSetCallbacks(m_Chat_cb, m_ConnLost_cb, m_AddPlayer_cb, m_DelPlayer_cb, m_Info_cb, m_Nudge_cb);


            m_Chat.AddInline(new Bold(new Run("Prompting for nickname.. ")));

            
            frmNickname formNick = new frmNickname();
            this.rectDisable.Visibility = System.Windows.Visibility.Visible;

            bool m_bConnectNow = false;

            if (!(m_Settings.bRememberName && m_Settings.bAutoConnect && m_Settings.strUsername.Length > 0))
            {
                if (m_Settings.strUsername.Length > 0)
                {
                    formNick.txtNick.Text = m_Settings.strUsername;
                    formNick.txtNick.SelectAll();
                }

                formNick.chkRememberName.IsChecked = m_Settings.bRememberName;
                formNick.chkDontConnect.IsChecked = !m_Settings.bAutoConnect;

                formNick.ShowDialog();

                if (!formNick.m_bValid)
                {
                    this.Close();
                    return;
                }

                m_Settings.strUsername = formNick.m_strNickname;
                m_Settings.bAutoConnect = !(bool)formNick.chkDontConnect.IsChecked;
                m_Settings.bRememberName = (bool)formNick.chkRememberName.IsChecked;
                m_Settings.Save();

                m_bConnectNow = formNick.m_bConnect;
            }
            else
            {
                m_bConnectNow = m_Settings.bAutoConnect;
                m_Settings.strUsername = m_Settings.strUsername;
            }

            
            this.rectDisable.Visibility = System.Windows.Visibility.Collapsed;
            this.lblRoomTitle.Content = " From '" + m_Settings.strUsername + "' to War Room";
            //this.mnuDebug.IsChecked = (bool)formNick.chkDebug.IsChecked;
            this.InvalidateVisual();
            m_Chat.AddInline(new Run(m_Settings.strUsername + "\n"));
            this.txtSend.Focus();

            if (m_bConnectNow)
            {
                m_Chat.InsertStatusMessage("Connecting to " + m_Settings.strServer + "..", Brushes.Black);

                m_ANet.SetServer(m_Settings.strServer);
                m_ANet.OpenDefault(GetUsername(), m_Conn_cb, m_ID_cb);
                
                mnuConnect.IsEnabled = false;
            }
            else
            {
                mnuConnect.IsEnabled = true;
                m_Chat.InsertStatusMessage("Ready. Use File > Connect to establish connection.", Brushes.Black);
            }

            btnMute.IsChecked = !m_Settings.bPlaySounds;

            Image imgAvail = new Image();
            Image imgDND = new Image();
            Image imgAway = new Image();
            Image imgRTG = new Image();

            imgAvail.Height = imgDND.Height = imgAway.Height = imgRTG.Height = 16;
            imgAvail.Source = m_imgUserBlue;
            imgDND.Source = m_imgUserDel;
            imgAway.Source = m_imgFlagBlue;
            imgRTG.Source = m_imgUserGreen;

            cmbStatus_Available.Content = imgAvail;
            cmbStatus_DND.Content = imgDND;
            cmbStatus_Away.Content = imgAway;
            cmbStatus_RTG.Content = imgRTG;
        }


        ~frmMain()
        {
            
        }

       /* public void AddInline(Inline item)
        {
            m_flowParagraph.Inlines.Add(item);
            txtChat.ScrollToEnd();
        }*/

        private string GetUsername()
        {
            return "Ð~" + m_Settings.strUsername;
        }

        private void AddMessage(String strUsername, String strMessage)
        {
            AddMessage(strUsername, strMessage, false);
        }

        private void AddMessage(String strUsername, String strMessage, bool bLocalUser)
        {
            SolidColorBrush brushUser;
            if (bLocalUser)
                brushUser = Brushes.DarkViolet;
            else
                brushUser = Brushes.Blue;

            m_Chat.InsertChatMessage(strUsername, strMessage, brushUser, Brushes.Black);
        }

        private void SendChatText(String strMessage)
        {
            if (strMessage.Equals("") || !m_ANet.isInited)
                return;

            bool bResult = m_ANet.SendChatMessage(txtSend.Text);

            AddMessage(GetUsername(), strMessage, true);
            if (bResult)
            {
                txtSend.Text = "";  // Clear send box
                if (PlayOutSound())
                    m_sndOutMsg.Play();
            }
            else
                m_Chat.InsertStatusMessage("Unable to send message.", Brushes.Red);
        }

        private void Connection(bool bSuccess)
        {
            if (bSuccess)
            {
                m_Chat.InsertStatusMessage("Connection success!", Brushes.DarkGreen);
                m_ANet.anEnumSessions(m_EnumSessions_cb);

                UpdateOptions();
            }
            else
                m_Chat.InsertStatusMessage("Connection failure..", Brushes.DarkRed);
        }


        private void ConnectionLost()
        {
            m_Chat.InsertStatusMessage("Connection lost!", Brushes.DarkRed);
        }

        private void ChatMessageReceived(int iFromID, string strLine)
        {
            StringBuilder sb = new StringBuilder(64);
            m_ANet.anGetPlayerName(iFromID, sb, 64);

            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                    AddMessage(sb.ToString(), strLine, false);

                    if (m_Settings.bFlashOnMsg && !this.IsActive)
                        FlashWindow.Flash(this, 1);
                }
            ));

            if (PlayInSound())
                m_sndInMsg.Play();


        }

        private PlayerInfo GetPlayerInfo(ListBox lstBox, int iIndex)
        {
            if (iIndex < 0)
                return null;
            StackPanel sp = (StackPanel)lstBox.Items.GetItemAt(iIndex);
            Label lbl = (Label)sp.Children[1];
            return (PlayerInfo)lbl.Content;
        }

        private int GetListIndex(ListBox lstBox, String strEntry)
        {
            for (int i = 0; i < lstBox.Items.Count; i++)
            {
                if (GetPlayerInfo(lstBox, i).ToString().Equals(strEntry.ToString()))
                    return i;
            }
            return -1;
        }

        private int GetListIndex(ListBox lstBox, int iPlayerID)
        {
            for (int i = 0; i < lstBox.Items.Count; i++)
            {
                if (GetPlayerInfo(lstBox, i).m_player.iID == iPlayerID)
                    return i;
            }
            return -1;
        }

        private void AddPlayer(Player p)
        {
            m_Chat.InsertJoinMessage(p.pName, Brushes.Blue, Brushes.Green);

           StackPanel sp = new StackPanel();
            sp.Orientation = System.Windows.Controls.Orientation.Horizontal;
            Label lbl = new Label();
            lbl.Padding = new Thickness(0);
            //lbl.Content = p.pName;
            lbl.Content = new PlayerInfo(p);

            Image img = new Image();
            if (p.pName.StartsWith("Ð")) // DROM user
                img.Source = m_imgUserBlue;
            else // Assuming in-game player
                img.Source = m_imgJoystick;

            img.Height = 16;

            sp.Children.Add(img);
            sp.Children.Add(lbl);
            lstUsers.Items.Add(sp);

           // lstUsers.Items.Add(new PlayerInfo(p));
            
            

            // TODO: Sort list ?

            m_ANet.anEnumSessions(m_EnumSessions_cb); // TODO: Cleanup
        }

        private void DelPlayer(Player p)
        {
            m_Chat.InsertLeaveMessage(p.pName, Brushes.Blue, Brushes.OrangeRed);

            int index = GetListIndex(lstUsers, p.pName);
            if (index == -1)
                m_Chat.InsertStatusMessage("(?) Couldn't remove userlist item: " + p.pName, Brushes.DarkRed);
            else
                lstUsers.Items.RemoveAt(index);

            m_ANet.anEnumSessions(m_EnumSessions_cb); // TODO: Cleanup
        }

        private void NudgeWindow()
        {
            //http://www.codeproject.com/KB/cs/msnnudge.aspx 

            // Store the original location of the form.

            int xCoord = (int)this.Left;
            int yCoord = (int)this.Top;
            
            // An integer for storing the random number each time

            int rnd = 0;

            // Instantiate the random generation mechanism

            Random RandomClass = new Random();

            for (int i = 0;  i <= 25; i++)
            {
                rnd = RandomClass.Next(xCoord + 1, xCoord + 10);
                this.Left = rnd;
                rnd = RandomClass.Next(yCoord + 1, yCoord + 10);
                this.Top = rnd;
            }

            // Restore the original location of the form

            this.Left = xCoord;
            this.Top = yCoord;
    
        }

        private void NudgeReceived(string strSender, int iSrcID)
        {
            if (PlayNudge())
            {
                m_Chat.InsertTimestamp(Brushes.Black);
                string strMessage = " nudged you!\n";
                Run run1 = new Run(" ** ");
                m_Chat.AddInline(run1);
                m_Chat.InsertUsername(strSender, Brushes.Violet, true, true);
                Italic run2 = new Italic(new Run(strMessage));
                run2.Foreground = Brushes.Violet;
                m_Chat.AddInline(run2);

                if (m_Settings.bFlashOnMsg && !this.IsActive)
                    FlashWindow.Flash(this, 1);

                if (PlaySounds() && m_sndNudge != null)
                {
                    m_sndNudge.Play();
                    NudgeWindow();
                }

                // Broadcast nudge info
                this.Dispatcher.Invoke(
                  System.Windows.Threading.DispatcherPriority.Normal,
                  new Action(
                    delegate()
                    {
                        int iNudgeBits = (1 << (int)NudgeBits.Info);
                        m_ANet.anNudgePlayer(-1, iSrcID, m_iID, iNudgeBits);
                    }
                ));
            }
            else // Nudges disabled
            {
                this.Dispatcher.Invoke(
                  System.Windows.Threading.DispatcherPriority.Normal,
                  new Action(
                    delegate()
                    {
                        int iNudgeBits = (1 << (int)NudgeBits.Disabled);
                        m_ANet.anNudgePlayer(iSrcID, m_iID, iSrcID, iNudgeBits);
                    }
                ));
            }
        }

        private void NudgeInfoReceived(string strSender, string strReceiver)
        {
            // TODO: Insert nudge message function into UserUtils
            m_Chat.InsertTimestamp(Brushes.Black);
            string strMessage = " nudges ";
            Run run1 = new Run(" ");
            m_Chat.AddInline(run1);
            m_Chat.InsertUsername(strSender, Brushes.Orchid, true, true);
            Italic run2 = new Italic(new Run(strMessage));
            run2.Foreground = Brushes.Orchid;
            m_Chat.AddInline(new Italic(run2));
            m_Chat.InsertUsername(strReceiver, Brushes.Orchid, true, true);
            Run run3 = new Run("\n");
            m_Chat.AddInline(run3);
        }

        private void Nudge_cb(int iFromID, int iSrcID, int iDestID, int iNudgeBits)
        {
            string strDest, strSource, strFrom;
            PlayerInfo piSource = GetPlayerInfo(lstUsers, GetListIndex(lstUsers, iSrcID));
            PlayerInfo piFrom = GetPlayerInfo(lstUsers, GetListIndex(lstUsers, iFromID));
            PlayerInfo piDest = GetPlayerInfo(lstUsers, GetListIndex(lstUsers, iDestID));

            if (iDestID == m_iID)
                //strDest = m_Settings.strUsername;
                strDest = " you";
            else
            {
                if (piDest == null)
                    strDest = "(unknown)";
                else
                    strDest = piDest.m_player.pName;
            }

            if (piSource == null)
                strSource = "(unknown)";
            else
                strSource = piSource.m_player.pName;
            if (piFrom == null)
                strFrom = "(unknown)";
            else
                strFrom = piFrom.m_player.pName;

            // TODO: Use m_Chat.InsertUsername

            if ((iNudgeBits & (1 << (int)NudgeBits.Info)) > 0 && iDestID != m_iID) // Information about a nudge
            {
                NudgeInfoReceived(strSource, strDest);
            }
            else if ((iNudgeBits & (1 << (int)NudgeBits.Disabled)) > 0) // User's nudging is disabled
            {
                m_Chat.InsertTimestamp(Brushes.Black);
                string strMessage = " has disabled nudges\n";
                Run run1 = new Run(" ");
                m_Chat.InsertUsername(strFrom, Brushes.MidnightBlue, true, false);
                Bold run = new Bold(new Run(strMessage));
                run.Foreground = Brushes.MidnightBlue;
                m_Chat.AddInline(run); 
            }
            else if ((iNudgeBits & (1 << (int)NudgeBits.Direct)) > 0) // Someone nudged us
            {
                NudgeReceived(strSource, iSrcID);
            }
        }

        private void Info_cb(int iFromID, int iCliVer, int iStatus, int iIdleMin, int iOnlineMin, int iOptions)
        {
            if (mnuDebug.IsChecked)
            {
                string strOut = "[DEBUG] Got player #" + iFromID + " info. Ver: " + iCliVer + ", status: " + iStatus + ", idle: " + iIdleMin + ", online: " + iOnlineMin + ", options: " + iOptions + "\n";
                m_Chat.AddInline(new Run(strOut));
            }

            
            int iIndex = GetListIndex(lstUsers, iFromID);
            if (iIndex == -1)
                return;

            PlayerInfo ps = GetPlayerInfo(lstUsers, iIndex);
            if (ps.iStatus != iStatus)
            {
                StackPanel sp = (StackPanel)lstUsers.Items.GetItemAt(iIndex);
                Image img = new Image();
                img.Height = 16;
                string strStatus = null;

                if (sp.Children.Count > 2)  // Remove status label
                    sp.Children.RemoveAt(2);

                switch (iStatus)
                {
                    case (int)UserStatus.Available:
                        img.Source = m_imgUserBlue;
                        break;
                    case (int)UserStatus.DND:
                        img.Source = m_imgUserDel;
                        strStatus = " (DND)";
                        break;

                    case (int)UserStatus.Away:
                        img.Source = m_imgFlagBlue;
                        strStatus = " (Away)";
                        break;

                    case (int)UserStatus.RTG:
                        img.Source = m_imgUserGreen;
                        break;

                }

                sp.Children.RemoveAt(0);
                sp.Children.Insert(0, img);

                if (strStatus != null)
                {
                    Label lblStatus = new Label();
                    lblStatus.Padding = new Thickness(0);
                    lblStatus.Content = strStatus;
                    sp.Children.Insert(2, lblStatus);
                }

                ps.iStatus = iStatus;
            }

            ps.iOptions = iOptions;

            ps.iCliVer = iCliVer;
            ps.iIdleMin = iIdleMin;
            ps.iOnlineMin = iOnlineMin;
            ps.bExtraInfo = true;

        }

        private void EnumSessions(Session[] SessList, int iSize)
        {
            this.lstRooms.Items.Clear();
            Session sess1, sess2;
            for (int i = 0; i < iSize; i++)
            {
                bool bAdd = true;
                sess1 = SessList[i];
                for (int j = i+1; j < iSize; j++)
                {
                    sess2 = SessList[j];
                    if (sess1.pName.Equals(sess2.pName))
                    {
                        bAdd = false;
                        break;
                    }
                }
                if (bAdd)
                    lstRooms.Items.Add(new RoomInfo(sess1));
            }

            return;
        }

        private void EnumSessPlayers(Player[] pPlayerList, int iSize)
        {
            if (iSize == 0)
                m_Chat.InsertStatusMessage("No players in room.", Brushes.MidnightBlue);
            else
                m_Chat.InsertStatusMessage(iSize + " players in room: ", Brushes.MidnightBlue);

            for (int i = 0; i < pPlayerList.Length; i++)
            {
                m_Chat.AddInline(new Bold(new Run("  " + (i+1))));
                m_Chat.AddInline(new Run(": "));
                m_Chat.InsertUsername(pPlayerList[i].pName, Brushes.Blue, false, false);
                m_Chat.AddInline(new Run("\n"));
            }

        }

        private void PingPlayerCallback(int iKarma, int iPingms, int iLoss)
        {

            // Find list item
            ListBox lstBox = this.lstUsers;
            PlayerInfo player = null;

            StackPanel sp = null;// (StackPanel)lstUsers.SelectedItem;
            Label lbl = null;

            for (int i = 0; i < lstBox.Items.Count; i++)
            {
                sp = (StackPanel)lstBox.Items.GetItemAt(i);
                lbl = (Label)sp.Children[1];
                player = (PlayerInfo)lbl.Content;
                if (iKarma == player.iKarma)
                    break;
            }

            if (player == null)
                return; // invalid player?

            player.iKarma = 0;

            m_Chat.InsertStatusMessage(player.m_player.pName + " ping: " + iPingms + "ms, loss: " + iLoss + "%", Brushes.MidnightBlue);


        }

        public void ID_cb(int iID)
        {
            m_iID = iID;    // Got an ID!
        }

        public void Debug_cb(String strLine)
        {
            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                   /* myCheckBox.IsChecked = true;
                    bDebug = mnuDebug.IsChecked;*/
                    if (mnuDebug.IsChecked)
                        m_Chat.AddInline(new Run(strLine + "\n"));
                }
            ));
        }

        private void frmMain_Closing(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_ANet.Dispose();

            Dispatcher.InvokeShutdown();

        }

        private void testSend(string strLine)
        {


        }


        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            SendChatText(txtSend.Text);
        }

        private void mnuConnect_Click(object sender, RoutedEventArgs e)
        {
            mnuConnect.IsEnabled = false;

            m_Chat.InsertStatusMessage("Connecting to " + m_Settings.strServer + "..", Brushes.Black);

            m_ANet.SetServer(m_Settings.strServer);
            m_ANet.OpenDefault(GetUsername(), m_Conn_cb, m_ID_cb);
        }
        private void mnuDisconnect_Click(object sender, RoutedEventArgs e)
        {

        }
        private void mnuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        void mnuOptions_Click(object sender, RoutedEventArgs e)
        {
            frmOptions options = new frmOptions(m_Settings);
            options.ShowDialog();

            UpdateOptions();
        }

        private void lstUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnMute_Checked(object sender, RoutedEventArgs e)
        {
            Mute(true);

        }

        private void cmbStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // May seem silly but is done this way since it may change quite a bit in future..
            switch (cmbStatus.SelectedIndex)
            {
                case (int)UserStatus.Available: // Available
                    m_iStatus = 0;
                    break;
                case (int)UserStatus.DND: // Do Not Disturb
                    m_iStatus = 1;
                    break;
                case (int)UserStatus.Away: // Away
                    m_iStatus = 2;
                    break;
                case (int)UserStatus.RTG: // Ready To Game
                    m_iStatus = 3;
                    break;
            }
            if (m_ANet != null && m_ANet.isInited)
            {
                UpdateOptions();
                m_ANet.anSetCliInfo(m_iStatus, m_iOptions);
            }

            if (this.IsLoaded)
                lblCount.Visibility = Visibility.Visible;
        }

        private void cm_lstUsers_OnClosed(object sender, RoutedEventArgs e)
        {

        }

        private void cm_lstUsers_OnOpened(object sender, RoutedEventArgs e)
        {
            PlayerInfo pi = GetPlayerInfo(lstUsers, lstUsers.SelectedIndex);
            if (pi == null)
                return;


            //if ((pi.iOptions & (int)OptionBits.Nudge) > 0) // Nudges enabled?
            if (pi.bExtraInfo)
                cm_lstUsers_Nudge.IsEnabled = true;
            else
                cm_lstUsers_Nudge.IsEnabled = false;

            if (pi.m_player.iID == m_iID) // Disable Ping and Nudge for self
                cm_lstUsers_Nudge.IsEnabled = cm_lstUsers_Ping.IsEnabled = false;
            else
                cm_lstUsers_Ping.IsEnabled = true;
        }

        private void cm_lstUsers_ViewInfo_Click(object sender, RoutedEventArgs e)
        {

            int iIndex = lstUsers.SelectedIndex;
            if (iIndex < 0 || iIndex > lstUsers.Items.Count - 1)
                return;

            PlayerInfo pi = GetPlayerInfo(lstUsers, iIndex);

            frmInfo infoForm = new frmInfo(pi);
            infoForm.Show();


        }

        private void cm_lstUsers_Ping_Click(object sender, RoutedEventArgs e)
        {
            /* DUPLICATED CODE! */
            // TODO: Factor code into Ping function

            StackPanel sp = (StackPanel)lstUsers.SelectedItem;
            if (sp == null)
                return;

            Label lbl = (Label)sp.Children[1];
            PlayerInfo player = (PlayerInfo)lbl.Content;
            if (player == null)
                return;
            player.iKarma = (short)(player.m_player.iID + player.iKarma + new Random().Next(50));

            this.Dispatcher.Invoke(
              System.Windows.Threading.DispatcherPriority.Normal,
              new Action(
                delegate()
                {
                    m_Chat.InsertStatusMessage("Pinging " + player.m_player.pName + "..", Brushes.MidnightBlue);
                    m_ANet.anPingPlayer(player.m_player.iID, player.iKarma, m_PingPlayer_cb);
                }
            ));
        }

        private void btnMute_Unchecked(object sender, RoutedEventArgs e)
        {
            Mute(false);
        }

        private void cm_lstUsers_Nudge_Click(object sender, RoutedEventArgs e)
        {
            PlayerInfo pi = GetPlayerInfo(lstUsers, lstUsers.SelectedIndex);
            if (pi == null)
                return;

            if (!m_Settings.bPlayNudges)
            {
                m_Chat.InsertStatusMessage("You have disabled nudges", Brushes.MidnightBlue);
                return;
            }

            MessageBoxResult mbRes = MessageBox.Show("Nudge " + pi.m_player.pName + "?", "DROM - Nudge?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (mbRes == MessageBoxResult.No)
                return;

            //if (pi.m_player.iID == m_iID)
            //    return;

            int iNudgeBits = (1 << (int)NudgeBits.Direct);

            m_ANet.anNudgePlayer(pi.m_player.iID, m_iID, pi.m_player.iID, iNudgeBits);

        }

        private void btnLaunch_Click(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(m_Settings.strDRPath))
            {
                MessageBoxResult res = MessageBox.Show("The path set in DROM options appears to be invalid. Go there now?", "DROM - Invalid DR path", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (res == MessageBoxResult.Yes)
                {
                    frmOptions options = new frmOptions(m_Settings);
                    options.ShowDialog();

                    UpdateOptions();
                }

                return;
            }

            try
            {
                System.Diagnostics.Process.Start(m_Settings.strDRPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't run the set program. Please check the path set in Tools > Options", "DROM - Error running program", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

    }


}
