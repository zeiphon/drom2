﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DROM
{
    /// <summary>
    /// Interaction logic for frmInfo.xaml
    /// </summary>
    public partial class frmInfo : Window
    {
        private PlayerInfo m_PlayerInfo;
        private bool m_bClosing = false;

        public frmInfo(PlayerInfo pPlayerInfo)
        {
            InitializeComponent();

            this.LostFocus += new RoutedEventHandler(frmInfo_LostFocus);
            this.Deactivated += new EventHandler(frmInfo_Deactivated);
            this.Closing += new System.ComponentModel.CancelEventHandler(frmInfo_Closing);

            m_PlayerInfo = pPlayerInfo;

            rectBottom.Fill = SystemColors.ControlBrush;
        }

        void frmInfo_Deactivated(object sender, EventArgs e)
        {
            if (!m_bClosing)
                this.Close();
        }

        void frmInfo_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_bClosing = true;
        }

        void frmInfo_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!m_bClosing)
                this.Close();
        }

        private void LoadInfo()
        {
            lblNickTitle.Content = lblNickname.Content = m_PlayerInfo.m_player.pName;

            Visibility v = (m_PlayerInfo.bExtraInfo ? Visibility.Hidden : Visibility.Visible);
            rectNoInfo.Visibility = lblNoInfo.Visibility =
                rectNoInfoInner.Visibility = txtNoInfo.Visibility = v;

            if (!m_PlayerInfo.bExtraInfo)
                return;

            int iCliVer = (int)m_PlayerInfo.iCliVer;

            // Assuming 4-byte (32 bit) int
            int v1 = (int)(iCliVer & 0xFF000000) >> 24;
            int v2 = (int)(iCliVer & 0x00FF0000) >> 16;
            int v3 = (int)(iCliVer & 0x0000FF00) >> 8;
            int v4 = (int)(iCliVer & 0x000000FF);

            string strCliVers = v1 + "." + v2 + "." +
                v3 + " build " + v4;

            lblCliVers.Content = strCliVers;

            lblOnline.Content = m_PlayerInfo.iOnlineMin + " min";
            lblIdle.Content = m_PlayerInfo.iIdleMin + " min";

            switch (m_PlayerInfo.iStatus)
            {
                case (int)UserStatus.Available: // Available
                    lblStatus.Content = "Available";
                    break;
                case (int)UserStatus.DND: // Do Not Disturb
                    lblStatus.Content = "Do Not Disturb";
                    break;
                case (int)UserStatus.Away: // Away
                    lblStatus.Content = "Away";
                    break;
                case (int)UserStatus.RTG: // Ready To Game
                    lblStatus.Content = "Ready To Game";
                    break;
            }

            lblSound.Content = ((m_PlayerInfo.iOptions & (1 << (int)OptionBits.Sound)) > 0 ? "Enabled" : "Disabled");
            lblNudges.Content = ((m_PlayerInfo.iOptions & (1 << (int)OptionBits.Nudge)) > 0 ? "Enabled" : "Disabled");

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoadInfo();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadInfo();
        }
    }
}
