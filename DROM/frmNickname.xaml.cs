﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DROM
{
    /// <summary>
    /// Interaction logic for frmNickname.xaml
    /// </summary>
    public partial class frmNickname : Window
    {
        public string m_strNickname;
        public bool m_bValid = false;
        public bool m_bConnect = false;

        public frmNickname()
        {
            InitializeComponent();

            txtNick.GotFocus += new RoutedEventHandler(txtNick_GotFocus);

            btnOK.IsDefault = true;

            txtNick.Focus();
        }

        private void txtNick_GotFocus(object sender, RoutedEventArgs e)
        {
            if (txtNick.Text.Equals("enter nickname"))
                txtNick.Text = "";
        }

        private void txtNick_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            if (txtNick.Text == "" || txtNick.Text == " " || txtNick.Text == "   " || txtNick.Text == "  ")
            {
                System.Windows.MessageBox.Show("Please enter a nonempty nickname", "Invalid nickname");
                m_bValid = false;
                return;
            }

            m_strNickname = txtNick.Text;
            m_bValid = true;
            m_bConnect = !((bool)chkDontConnect.IsChecked);

            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            m_bValid = false;
            this.Close();
        }
    }
}
