﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

namespace DROM
{

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct Session
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst=32)]
        public string pName;
        public int iNumPlayers;
        public int iMaxPlayers;
        public bool bInProgress;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct Player
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst=32)]
        public string pName;
        public int iID;
    }

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void DebugCallback(string strLine);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void EnumSessionsCallback(
    [MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)]
    Session[] pSessList, int iSize);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void EnumPlayersCallback(
    [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)]
    Player[] pPlayerList, int iSize);
    
    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void EnumSessPlayersCallback(
    [MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 1)]
    Player[] pPlayerList, int iSize);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void AddPlayerCallback(Player player);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void DelPlayerCallback(Player player);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void ChatMessageCallback(int iFromID, string strLine);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void ConnectionLostCallback();

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void ConnectionCallback(bool bSuccess);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void PingPlayerCallback(int iKarma, int iPingms, int iLoss);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void NudgeCallback(int iFromID, int iSrcID, int iDestID, int iNudge);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void InfoCallback(int iFromID, int iCliVer, int iStatus, int iIdleMin, int iOnlineMin, int iOptions);

    [UnmanagedFunctionPointerAttribute(CallingConvention.StdCall)]
    public delegate void IDCallback(int iMyID);

    public class ANet : IDisposable
    {

       /* [DllImport("anetwrap.dll")]
        public static extern bool anInit(int iUpdateFreq, DebugCallback debugCallback);
        [DllImport("anetwrap.dll")]
        public static extern bool setKeepaliveFreq(int iFreq);
        [DllImport("anetwrap.dll")]
        public static extern bool anCreate(string strDllName);
        [DllImport("anetwrap.dll")]
        public static extern bool anSetGameServer(string strAddress, short sessType);
        [DllImport("anetwrap.dll")]
        public static extern bool anOpenDefault(string strUserName, ConnectionCallback connectionCallback);
        [DllImport("anetwrap.dll")]
        public static extern bool anSendChatMessage(string strMessage);
        [DllImport("anetwrap.dll")]
        public static extern bool anGetPlayerName(int iID, ref string strBuffer, int iBufSize);
        [DllImport("anetwrap.dll")]
        public static extern bool anIsConnected();
        [DllImport("anetwrap.dll")]
        public static extern bool anShutdown();
        [DllImport("anetwrap.dll")]
        public static extern void anSetCallbacks(ChatMessageCallback chatMessageCallback, ConnectionLostCallback connLostCallback,
            AddPlayerCallback addPlayerCallback, DelPlayerCallback delPlayerCallback);*/

        [DllImport("kernel32")]
        private extern static int LoadLibrary(string lpLibFileName);
        [DllImport("kernel32")]
        private extern static bool FreeLibrary(int hLibModule);
        [DllImport("kernel32", CharSet = CharSet.Ansi)]
        private extern static int GetProcAddress(int hModule, string lpProcName);

        public delegate bool anInit_d(int iCliVer, int iUpdateFreq, DebugCallback debugCallback);
        public delegate bool setKeepaliveFreq_d(int iFreq);
        public delegate bool anCreate_d(string strDllName);
        public delegate bool anSetGameServer_d(string strAddress, short sessType);
        public delegate bool anOpenDefault_d(string strUserName, ConnectionCallback connectionCallback, IDCallback idCallback);
        public delegate bool anSendChatMessage_d(string strMessage);
        public delegate bool anGetPlayerName_d(int iID, [In, Out] StringBuilder strBuffer, int iBufSize);
        public delegate bool anIsConnected_d();
        public delegate bool anShutdown_d();
        public delegate void anSetCallbacks_d(ChatMessageCallback chatMessageCallback, ConnectionLostCallback connLostCallback,
            AddPlayerCallback addPlayerCallback, DelPlayerCallback delPlayerCallback, InfoCallback infoCallback, NudgeCallback nudgeCallback);

        public delegate bool anEnumSessions_d(EnumSessionsCallback SessionsCallback);
        public delegate bool anEnumPlayers_d(EnumPlayersCallback PlayersCallback);
        public delegate bool anEnumSessPlayers_d(string strSessName, EnumSessPlayersCallback SessPlayersCallback);
        public delegate bool anPingPlayer_d(int iID, int iKarma, PingPlayerCallback PingCallback);
        public delegate bool anNudgePlayer_d(int iIDTo, int iSrcID, int iDestID, int iNudgeBits);
        public delegate bool anSetCliInfo_d(int iStatus, int iOptions);

        public anInit_d anInit;
        public anCreate_d anCreate;
        public anSendChatMessage_d anSendChatMessage;
        public anShutdown_d anShutdown;
        public anSetGameServer_d anSetGameServer;
        public anOpenDefault_d anOpenDefault;
        public anGetPlayerName_d anGetPlayerName;
        public anIsConnected_d anIsConnected;
        public anSetCallbacks_d anSetCallbacks;
        public anEnumPlayers_d anEnumPlayers;
        public anEnumSessions_d anEnumSessions;
        public anEnumSessPlayers_d anEnumSessPlayers;
        public anPingPlayer_d anPingPlayer;
        public anSetCliInfo_d anSetCliInfo;
        public anNudgePlayer_d anNudgePlayer;

        public static void Debug_cb(string strLine)
        {
/*            if (m_frmMain.mnuDebug.IsChecked)
                m_frmMain.AddInline(new System.Windows.Documents.Run(strLine + "\n"));*/
            m_frmMain.Debug_cb(strLine);
        }

        private static frmMain m_frmMain;
        private DebugCallback m_DebugCallback;

        private bool m_bInited = false;

        private int m_iDLL_h = 0;
        private int m_ianInit_h = 0;
        private int m_ianCreate_h = 0;
        private int m_ianShutdown_h = 0;
        private int m_ianSendChatMessage_h = 0;
        private int m_ianSetCallbacks_h = 0;
        private int m_ianOpenDefault_h = 0;
        private int m_ianSetGameServer_h = 0;
        private int m_ianGetPlayerName_h = 0;
        private int m_ianEnumSessions_h = 0;
        private int m_ianEnumPlayers_h = 0;
        private int m_ianEnumSessPlayers_h = 0;
        private int m_ianPingPlayer_h = 0;
        private int m_ianNudgePlayer_h = 0;
        private int m_ianSetCliInfo_h = 0;

        Mutex m_mutex;

        public ANet(frmMain mainForm)
        {
            m_frmMain = mainForm;
            m_DebugCallback = new DebugCallback(ANet.Debug_cb);

            m_mutex = new Mutex(true, "ANet mutex");

        }

        public bool isInited
        {
            get
            {
                return m_bInited;
            }
            private set
            {
                m_bInited = value;
            }
        }

        public bool Init(int iCliVer)
        {

            if (m_bInited)
                return false;

            //m_mutex.WaitOne();

            m_iDLL_h = LoadLibrary("anetwrap.dll");
            if (m_iDLL_h == 0)
            {
                throw new Exception("Unable to load anetwrap.dll");
            }

            m_ianInit_h = GetProcAddress(m_iDLL_h, "anInit");
            if (m_ianInit_h == 0) throw new Exception("Unable to hook anInit");

            anInit = (anInit_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianInit_h, typeof(anInit_d));

            m_ianCreate_h = GetProcAddress(m_iDLL_h, "anCreate");
            if (m_ianCreate_h == 0) throw new Exception("Unable to hook anCreate");
            anCreate = (anCreate_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianCreate_h, typeof(anCreate_d));

            m_ianShutdown_h = GetProcAddress(m_iDLL_h, "anShutdown");
            if (m_ianShutdown_h == 0) throw new Exception("Unable to hook anShutdown");
            anShutdown = (anShutdown_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianShutdown_h, typeof(anShutdown_d));

            m_ianSetCallbacks_h = GetProcAddress(m_iDLL_h, "anSetCallbacks");
            if (m_ianSetCallbacks_h == 0) throw new Exception("Unable to hook anSetCallbacks");
            anSetCallbacks = (anSetCallbacks_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianSetCallbacks_h, typeof(anSetCallbacks_d));

            m_ianOpenDefault_h = GetProcAddress(m_iDLL_h, "anOpenDefault");
            if (m_ianOpenDefault_h == 0) throw new Exception("Unable to hook anOpenDefault");
            anOpenDefault = (anOpenDefault_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianOpenDefault_h, typeof(anOpenDefault_d));

            m_ianSetGameServer_h = GetProcAddress(m_iDLL_h, "anSetGameServer");
            if (m_ianSetGameServer_h == 0) throw new Exception("Unable to hook anSetGameServer");
            anSetGameServer = (anSetGameServer_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianSetGameServer_h, typeof(anSetGameServer_d));

            m_ianSendChatMessage_h = GetProcAddress(m_iDLL_h, "anSendChatMessage");
            if (m_ianSendChatMessage_h == 0) throw new Exception("Unable to hook anSendChatMessage");
            anSendChatMessage = (anSendChatMessage_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianSendChatMessage_h, typeof(anSendChatMessage_d));

            m_ianGetPlayerName_h = GetProcAddress(m_iDLL_h, "anGetPlayerName");
            if (m_ianGetPlayerName_h == 0) throw new Exception("Unable to hook anGetPlayerName");
            anGetPlayerName = (anGetPlayerName_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianGetPlayerName_h, typeof(anGetPlayerName_d));

            m_ianEnumSessions_h = GetProcAddress(m_iDLL_h, "anEnumSessions");
            if (m_ianEnumSessions_h == 0) throw new Exception("Unable to hook anEnumSessions");
            anEnumSessions = (anEnumSessions_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianEnumSessions_h, typeof(anEnumSessions_d));

            m_ianEnumPlayers_h = GetProcAddress(m_iDLL_h, "anEnumPlayers");
            if (m_ianEnumPlayers_h == 0) throw new Exception("Unable to hook anEnumPlayers");
            anEnumPlayers = (anEnumPlayers_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianEnumPlayers_h, typeof(anEnumPlayers_d));

            m_ianEnumSessPlayers_h = GetProcAddress(m_iDLL_h, "anEnumSessPlayers");
            if (m_ianEnumSessPlayers_h == 0) throw new Exception("Unable to hook anEnumSessPlayers");
            anEnumSessPlayers = (anEnumSessPlayers_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianEnumSessPlayers_h, typeof(anEnumSessPlayers_d));

            m_ianPingPlayer_h = GetProcAddress(m_iDLL_h, "anPingPlayer");
            if (m_ianPingPlayer_h == 0) throw new Exception("Unable to hook anPingPlayer");
            anPingPlayer = (anPingPlayer_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianPingPlayer_h, typeof(anPingPlayer_d));
           
            m_ianSetCliInfo_h = GetProcAddress(m_iDLL_h, "anSetCliInfo");
            if (m_ianSetCliInfo_h == 0) throw new Exception("Unable to hook anSetCliInfo");
            anSetCliInfo = (anSetCliInfo_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianSetCliInfo_h, typeof(anSetCliInfo_d));
            
            m_ianNudgePlayer_h = GetProcAddress(m_iDLL_h, "anNudgePlayer");
            if (m_ianNudgePlayer_h == 0) throw new Exception("Unable to hook anNudgePlayer");
            anNudgePlayer = (anNudgePlayer_d)Marshal.GetDelegateForFunctionPointer((IntPtr)m_ianNudgePlayer_h, typeof(anNudgePlayer_d));

            anInit(iCliVer, 30, m_DebugCallback);
            anCreate("winets2.dll");

            //m_mutex.ReleaseMutex();

            m_bInited = true;
            return true;
        }

        public bool SetServer(string strAddress)
        {
            return anSetGameServer(strAddress, 669); // 669 = Dark Reign lobby
        }

        public bool OpenDefault(string strUserName, ConnectionCallback Connection_cb, IDCallback ID_cb)
        {
            return anOpenDefault(strUserName, Connection_cb, ID_cb);
        }

        public bool SendChatMessage(string strMessage)
        {
            if (m_iDLL_h == 0)
                return false;
            return anSendChatMessage(strMessage);
        }



        private bool disposing = false;
        private bool isDisposed = false;

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Code to dispose the managed resources of the class
            }
            // Code to dispose the un-managed resources of the class

            if (m_iDLL_h != 0)
                anShutdown();

            if (m_iDLL_h != 0)
                FreeLibrary(m_iDLL_h);

            isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        
    }
}
