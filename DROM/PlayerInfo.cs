﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DROM
{
    public class PlayerInfo
    {
        public Player m_player;
        public short iKarma
        { get; set; }

        public int iCliVer
        { get; set; }
        public int iStatus
        { get; set; }
        public int iOptions
        { get; set; }
        public int iIdleMin
        { get; set; }
        public int iOnlineMin
        { get; set; }

        public bool bExtraInfo
        { get; set; }

        public PlayerInfo(Player player)
        {
            m_player = player;
            iKarma = -1;
            bExtraInfo = false;

            iCliVer = iStatus = iOptions = iIdleMin = iOnlineMin = 0;
        }

        override public string ToString()
        {
            return m_player.pName;
        }

    }
}
